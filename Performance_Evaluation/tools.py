import numpy as np


## Dataset preprocessing

def preprocessData(df, columns=None):
    #change the variable types for low memory usage
    #int64 to int32,,, float64 to float32
    integer = []
    f = []
    for i in df.columns[:-1]:
        if df[i].dtype == "int64": 
            integer.append(i)
        elif df[i].dtype == "object":
            pass
        else: 
            f.append(i)

    df[integer] = df[integer].astype("int32")
    df[f] = df[f].astype("float32")

    if type(columns)==type(None):
        # drop one variable features 
        one_variable_list = []
        for i in df.columns:
            if df[i].value_counts().nunique() < 2:
                one_variable_list.append(i)
        if "Label" in one_variable_list:
            one_variable_list.remove("Label")

        df.drop(one_variable_list,axis=1,inplace=True)
        df.columns =  df.columns.str.strip()
    
        # drop nan and infinite rows
        df = df[~df.isin([np.nan, np.inf, -np.inf]).any(1)]
    
        # Remove all textual features
        to_drop = list(df.columns[(df.dtypes==object)])
        if "Label" in to_drop:
            to_drop.remove("Label")
        # Remove random feature Src port
        to_drop.append("Src Port")
        
        # Drop features 
        df.drop(to_drop, axis=1, inplace=True)
        columns = df.columns
        df_out = df.drop(list(df.columns[df.dtypes==object]),axis=1)        
    else:
        # drop nan and infinite rows
        df = df[~df.isin([np.nan, np.inf, -np.inf]).any(1)]
        df_out = df.loc[::, columns].drop(list(df.loc[::, columns].columns[df.loc[::, columns].dtypes==object]),axis=1)

    x = df_out.values
    y = df["Label"].values
    return x, y, columns


def shuffle(array1, array2):
    assert (array1.shape[0] == array2.shape[0]), "Array's first dimension should be the same"
    randomize = np.arange(array1.shape[0])
    np.random.shuffle(randomize)
    return (array1[randomize], array2[randomize])

