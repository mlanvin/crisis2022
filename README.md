# Errors in the CIC-IDS2017 dataset and the significant differences in detection performances it makes - CRiSIS 2022

## Description

This repository contains the code to get the results obtained in the article **_Errors in the CIC-IDS2017 dataset and the significant differences in detection performances it makes_** submitted at CRiSIS2022.

Through the article, we identified several issues both in the raw data (packet duplication, packets misordering and inconsistent timestamps) and in the labels (attack mislabelling). The provided code allow to fix the different issues. The folder `Pcap_without_duplication` contains the tool to remove the duplicated as we did in the article. `PortScan_Correction` folder includes notebooks to put the correct labels for the mislabelled attack.  

The whole content of the repository is the following one:
- `Labels` : this folder contains the labels of every experiment configurations that are described in the paper.
- `Pcap_without_duplication` : this folder contains the script to obtain the network capture files without duplication.
- `Performance_Evaluation` : this folder contains the notebooks to measure the performances of supervised models using relabelled datasets.
- `PortScan_Correction` : this folder contains the notebooks to add the port scan attack on original CSV files.    

## Run
To run all the experiments please execute the notebook in the following order:
1. `Evaluation_relabelling_CICIDS2017.ipynb`
2. `Evaluation_relabelling_CICIDS2017_reordered.ipynb`
3. `Evaluation_relabelling_CICIDS2017_reordered_without_duplication.ipynb`
4. `Results_Comparison.ipynb`
The last notebook uses the results produced by the others.


The notebooks that add Port Scan attack in the labels can be executed independently:
- `PortScan_Addition_CICIDS2017.ipynb`
- `PortScan_Addition_Joosen.ipynb` 

To obtain the network captures without duplicated packets run the script in `Pcap_without_duplication` after putting the original CICIDS2017 pcap files in the folder `Original_CICIDS2017_pcap`. The outputs are created in the folder : `WithoutDuplication_CICIDS2017_pcap`.

